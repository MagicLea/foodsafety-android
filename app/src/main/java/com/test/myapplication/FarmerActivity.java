package com.test.myapplication;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;

public class FarmerActivity  extends AppCompatActivity {

    /**
     * Keep track of the task to ensure we can cancel it if requested.
     */
    private FarmerTask mFarmerTask = null;

    // UI references.
    private EditText mFieldIDView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer);

        mFieldIDView = (EditText) findViewById(R.id.field_id);
        mFieldIDView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptScan();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.scan_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptScan();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            String scanResult = data.getStringExtra("QRcode");
            String fieldID = data.getExtras().getString("fieldID");
            mFarmerTask = new FarmerTask(mFieldIDView.getText().toString(), scanResult);
            mFarmerTask.execute((Void) null);
        }
        else {
            Toast.makeText(FarmerActivity.this,"掃描已取消",Toast.LENGTH_SHORT).show();
            showProgress(false);
        }
    }

    private void attemptScan() {
        if (mFarmerTask != null) {
            return;
        }

        // Reset errors.
        mFieldIDView.setError(null);

        String fieldID = mFieldIDView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(fieldID)) {
            mFieldIDView.setError(getString(R.string.error_field_required));
            focusView = mFieldIDView;
            cancel = true;
        } else if (!isFieldIDValid(fieldID)) {
            mFieldIDView.setError("fieldID是由純數字組成");
            focusView = mFieldIDView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            Intent intent = new Intent();
            intent.putExtra("fieldID",fieldID);
            intent.setClass(FarmerActivity.this,ScannerActivity.class);
            startActivityForResult(intent, 0);
        }
    }

    private boolean isFieldIDValid(String fieldID) {
        return fieldID.matches("[0-9]+");
    }

    /**
     * Shows the progress UI and hides the  form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous task used to authenticate
     * the user.
     */
    public class FarmerTask extends AsyncTask<Void, Void, Integer> {

        private final String mFieldID;
        private final String mQRcode;

        FarmerTask(String fieldID, String QRcode) {
            mFieldID = fieldID;
            mQRcode = QRcode;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            HttpURLConnection conn = null;
            BufferedReader bufferedReader;
            try {
                URL url = new URL(String.format("http://140.116.247.20:4280/foodsafety/addLog.php?field_id=%s&qr_code=%s", mFieldID, mQRcode));
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String tempStr;
                StringBuilder stringBuilder = new StringBuilder();

                while ((tempStr = bufferedReader.readLine()) != null) {
                    stringBuilder.append(tempStr);
                }
                Log.d("FarmerTask",stringBuilder.toString());
                final ResponseObj response;
                response = new Gson().fromJson(stringBuilder.toString(),ResponseObj.class);
                if(response.error != 0) {
                    FarmerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(FarmerActivity.this,response.msg,Toast.LENGTH_SHORT).show();
                        }
                    });
                    Log.d("FarmerTask",response.msg);
                }
                return response.error;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return -1;
        }

        @Override
        protected void onPostExecute(final Integer error) {
            mFarmerTask = null;
            showProgress(false);

            if (error == 0) {
                Toast.makeText(FarmerActivity.this,"新增紀錄成功",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mFarmerTask = null;
            showProgress(false);
        }

        private class ResponseObj {
            int error;
            String msg;
        }
    }
}
