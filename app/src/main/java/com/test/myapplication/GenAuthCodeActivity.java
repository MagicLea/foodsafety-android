package com.test.myapplication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A screen that offers to generate QR code.
 */
public class GenAuthCodeActivity extends AppCompatActivity {

    /**
     * Keep track of the task to ensure we can cancel it if requested.
     */
    private GenAuthCodeTask mGenTask = null;

    // UI references.
    private EditText mTargetView;
    private EditText mNumView;
    private View mProgressView;
    private View mGenFormView;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen_auth_code);
        // Set up the form.
        mTargetView = (EditText) findViewById(R.id.target_id);
        mTargetView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    mNumView.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mNumView = (EditText) findViewById(R.id.gen_num);
        mNumView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.gen || id == EditorInfo.IME_NULL) {
                    attemptGen();
                    return true;
                }
                return false;
            }
        });

        Button mGenButton = (Button) findViewById(R.id.gen_button);
        mGenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptGen();
            }
        });

        mGenFormView = findViewById(R.id.gen_form);
        mProgressView = findViewById(R.id.gen_progress);
        mLinearLayout = (LinearLayout)findViewById(R.id.linear_layout);
    }


    /**
     * Attempts to generate QR code specified by the form.
     * If there are form errors , the
     * errors are presented and no actual attempt is made.
     */
    private void attemptGen() {
        if (mGenTask != null) {
            return;
        }

        // Reset errors.
        mTargetView.setError(null);
        mNumView.setError(null);

        // Store values at the time of the attempt.
        String target = mTargetView.getText().toString();
        String numStr = mNumView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid target_id.
        if (TextUtils.isEmpty(target)) {
            mTargetView.setError(getString(R.string.error_field_required));
            focusView = mTargetView;
            cancel = true;
        } else if (!isTargetValid(numStr)) {
            mTargetView.setError(getString(R.string.error_invalid_account));
            focusView = mTargetView;
            cancel = true;
        }

        // Check for a valid amount.
        if (TextUtils.isEmpty(numStr)) {
            mNumView.setError(getString(R.string.error_field_required));
            focusView = mNumView;
            cancel = true;
        } else if (!isNumStrValid(numStr)) {
            mNumView.setError(getString(R.string.error_invalid_password));
            focusView = mNumView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt generating QR code and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the generating QR code attempt.
            showProgress(true);
            mGenTask = new GenAuthCodeTask(target, Integer.valueOf(numStr));
            mGenTask.execute((Void) null);
        }
    }

    private boolean isTargetValid(String target) {
        return target.matches("[A-Za-z0-9]+");
    }

    private boolean isNumStrValid(String numStr) {
        return numStr.matches("[0-9]+");
    }

    /**
     * Shows the progress UI and hides the form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mGenFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mGenFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mGenFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mGenFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class GenAuthCodeTask extends AsyncTask<Void, Void, GenAuthCodeTask.ResponseObj> {

        private final String mTargetID;
        private final int mNum;

        GenAuthCodeTask(String targetID, int num) {
            mTargetID = targetID;
            mNum = num;
        }

        @Override
        protected ResponseObj doInBackground(Void... params) {
            HttpURLConnection conn = null;
            BufferedReader bufferedReader = null;
            try {
                URL url = new URL(String.format("http://140.116.247.20:4280/foodsafety/genAuthCode.php?target_id=%s&num=%d",mTargetID,mNum));
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String tempStr;
                StringBuilder stringBuilder = new StringBuilder();

                while ((tempStr = bufferedReader.readLine()) != null) {
                    stringBuilder.append(tempStr);
                }
                Log.d("GenAuthCodeTask",stringBuilder.toString());
                final ResponseObj response;
                response = new Gson().fromJson(stringBuilder.toString(),ResponseObj.class);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final ResponseObj response) {
            mGenTask = null;
            showProgress(false);

            if (response.error == 0) {
                Toast.makeText(GenAuthCodeActivity.this,getString(R.string.gen_successful),Toast.LENGTH_SHORT).show();
                for(final String qrcode : response.qrcodes) {
                    ImageView img = new ImageView(GenAuthCodeActivity.this);
                    img.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(GenAuthCodeActivity.this, qrcode,Toast.LENGTH_SHORT).show();
                        }
                    });
                    Glide.with(GenAuthCodeActivity.this).load(String.format("http://chart.apis.google.com/chart?cht=qr&chl=%s&chs=200x200&chld=H", qrcode)).into(img);
                    mLinearLayout.addView(img);
                }
            } else if(response.error == 3){
                mTargetView.setError(getString(R.string.error_incorrect_target_id));
                mTargetView.requestFocus();
            }
            else {
                mNumView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mGenTask = null;
            showProgress(false);
        }

        protected class ResponseObj {
            int error;
            String msg;
            String[] qrcodes;
        }
    }
}

