package com.test.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GuestActivity extends AppCompatActivity {

    // UI references.
    private EditText mFieldIDView;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        mFieldIDView = (EditText) findViewById(R.id.field_id);
        mWebView = (WebView) findViewById(R.id.webView);

        Button mSignInButton = (Button) findViewById(R.id.scan_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(GuestActivity.this,ScannerActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            String scanResult = data.getStringExtra("QRcode");
            mFieldIDView.setText(scanResult);
            attemptQuery();
//            mFarmerTask = new FarmerTask(mFieldIDView.getText().toString(), scanResult);
//            mFarmerTask.execute((Void) null);
        }
        else {
            Toast.makeText(GuestActivity.this,"掃描已取消",Toast.LENGTH_SHORT).show();
        }
    }

    private void attemptQuery() {

        // Reset errors.
        mFieldIDView.setError(null);

        String qrCode = mFieldIDView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(qrCode)) {
            mFieldIDView.setError(getString(R.string.error_field_required));
            focusView = mFieldIDView;
            cancel = true;
        } else if (!isFieldIDValid(qrCode)) {
            mFieldIDView.setError("這不是一個正確的QR code");
            focusView = mFieldIDView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(String.format("http://140.116.247.20:4280/foodsafety/logs.php?qr_code=%s",qrCode));
        }
    }

    private boolean isFieldIDValid(String fieldID) {
        return fieldID.matches("[0-9]+-[A-F0-9]{6}");
    }
}
